export interface RecipeInterface {
    
    id: number;
    name: string;
    category: string;
    image: string;
    score: number;

    getId(): number;

    getName(): string;
    setName(name:string): void;

    getCategory(): string;
    setCategory(category:string): void;

    getImage(): string;
    setImage(image:string): void;

    getScore(): number;
    setScore(score:number): void;

}