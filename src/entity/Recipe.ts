export class Recipe {
    constructor(
        private name: string,
        private category: string,
        private image: string,
        private score: number,
        private id?: number
    ) { }

    getName(): string {
        return this.name
    }
    getCategory(): string {
        return this.category
    }
    getImage(): string {
        return this.image
    }
    getScore(): number {
        return this.score
    }
    getId() {
        return this.id;
    }
    setId(id: number) {
        this.id = id;
    }

}