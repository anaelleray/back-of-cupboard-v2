export class Ingredient {
    constructor(
        private name: string,
        private id?: number
    ) { }

    getName() {
        return this.name;
    }
    getId() {
        return this.id;
    }
    setId(id: number) {
        this.id = id;
    }
}